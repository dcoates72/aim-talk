Slides (using Beamer w/Metropolis Theme) of talk given Sept. 29 2015 for KU Leuven AI Master's Students project proposals.

More technical details on likely code:

- CNNs using Caffe&Torch, or possibly Theano (Python)
- Shock graphs's using Feldman and Singh's Bayesian Method: [their paper](http://www.pnas.org/content/103/47/18014.short)

We will try to reuse code, but re-implementing is of course possible for interested students, since it is the best way to learn.

See [latest version PDF](https://bitbucket.org/dcoates72/aim-talk/raw/master/coates_aim_pitch.pdf)