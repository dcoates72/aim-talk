proj=coates_aim_pitch

all:
	pdflatex $(proj).tex
	bibtex $(proj)
	pdflatex $(proj).tex
	pdflatex $(proj).tex

dimAs:
	convert img/spirit_as.png -alpha on -channel a -evaluate set 5% img/spirit_as_dim.png

clean:
	rm -f $(proj).vrb $(proj).snm $(proj).out $(proj).aux $(proj).toc $(proj).nav $(proj).log $(proj).bbl $(proj).blg

view:
	evince coates_aim_pitch.pdf 2>/dev/null &
